package GraphicalUnit;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Graphics {
    private String[] numberOfServers = {"1","2","3","4","5","6"};
    private JFrame frame = new JFrame();
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();



    private JTextArea[] servers=new JTextArea[6];
    private JTextField[] contents = new JTextField[6];
    private JButton start = new JButton("START");
    private JTextArea server = new JTextArea("Nr Servere:");
    private JTextField time1 = new JTextField("Min Time");
    private JTextField time2 = new JTextField("Max Time");
    private JTextArea procTime=new JTextArea("<=Time<=");
    private JComboBox box = new JComboBox(numberOfServers);
    private JTextField timeLimit = new JTextField("Time Limit");
    private JTextField clients = new JTextField("Nr of clients");
    private JTextArea log= new JTextArea(30,75);
    private JScrollPane jsp = new JScrollPane(log);
    public Graphics()
    {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800,800);
        frame.setResizable(false);

        time1.setPreferredSize(new Dimension(60,20));
        time2.setPreferredSize(new Dimension(60,20));
        timeLimit.setPreferredSize(new Dimension(60,20));
        clients.setPreferredSize(new Dimension(80,20));

        panel1.setLayout(new FlowLayout(0));
        panel1.add(start);
        server.setEditable(false);
        server.setBackground(Color.CYAN);
        panel1.add(server);
        panel1.add(box);
        procTime.setEditable(false);
        procTime.setBackground(Color.CYAN);
        panel1.add(time1);
        panel1.add(procTime);
        panel1.add(time2);
        panel1.add(timeLimit);
        panel1.add(clients);
        panel1.setBackground(Color.CYAN);

        panel2.setLayout(new FlowLayout());
        panel2.setBackground(Color.BLUE);
        panel2.add(jsp);

        panel3.setLayout(new FlowLayout(0));
        panel3.setBackground(Color.BLUE);


        frame.add(panel1,BorderLayout.NORTH);
        frame.add(panel2,BorderLayout.SOUTH);
        frame.add(panel3,BorderLayout.CENTER);
        frame.setVisible(true);
    }

    public JTextField getTime1() {
        return time1;
    }

    public JTextField getTime2() {
        return time2;
    }

    public JComboBox getBox() {
        return box;
    }

    public JTextField getTimeLimit() {
        return timeLimit;
    }

    public JTextField getClients() {
        return clients;
    }

    public JButton getStart() {
        return start;
    }

    public JTextArea getLog() {
        return log;
    }

    public JTextArea[] getServers() {
        return servers;
    }

    public JTextField[] getContents() {
        return contents;
    }

    public JPanel getPanel3() {
        return panel3;
    }

    public JFrame getFrame() {
        return frame;
    }
}
