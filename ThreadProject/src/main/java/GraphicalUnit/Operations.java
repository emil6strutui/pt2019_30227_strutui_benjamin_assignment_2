package GraphicalUnit;

import TaskManager.SimulationManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Operations extends Graphics {
    public Operations() {
        this.startButton();
    }

    private void startButton() {
        getStart().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int valid = 0;

                getLog().setText("");
                valid += isValid(getTime1(), "Min Time");
                valid += isValid(getTime2(), "Max Time");
                valid += isValid(getTimeLimit(), "Time Limit");
                valid += isValid(getClients(), "Nr of clients");
                if (valid == 0) {
                    getStart().setEnabled(false);
                    if(getPanel3().getComponents().length>0)
                    {
                        removeServers(getPanel3().getComponents().length/2);
                    }
                    makingServers(Integer.parseInt((String) getBox().getSelectedItem()));
                    SimulationManager a = new SimulationManager(Integer.parseInt(getTimeLimit().getText()), Integer.parseInt(getTime2().getText()), Integer.parseInt(getTime1().getText()),
                            Integer.parseInt(getClients().getText()), Integer.parseInt((String) getBox().getSelectedItem()), getLog(),getContents(),getStart());
                    (new Thread(a)).start();
                } else
                    JOptionPane.showMessageDialog(null, "Reintroduce the values correctly!", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private int isValid(JTextField a, String b) {
        try {
            Integer.parseInt(a.getText());
            return 0;
        } catch (NumberFormatException ex) {
            a.setText(b);
            JOptionPane.showMessageDialog(null, "The " + b + " field must be a number!", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return -1;
    }

    private void makingServers(int servers) {
       getFrame().setVisible(false);
        for (int i = 1; i <= servers; i++) {
            this.getServers()[i-1]=new JTextArea("Server" + i);
            this.getServers()[i-1].setEditable(false);
            this.getContents()[i-1]=new JTextField("");
            this.getContents()[i-1].setPreferredSize(new Dimension(700,20));
            this.getContents()[i-1].setEditable(false);
            this.getPanel3().add(getServers()[i-1]);
            this.getPanel3().add(getContents()[i-1]);
        }

        getFrame().setVisible(true);
    }
    private void removeServers(int servers)
    {
        for(int i=0;i<servers;i++)
        {
            getPanel3().remove(getContents()[i]);
            getPanel3().remove(getServers()[i]);
        }
    }

}
