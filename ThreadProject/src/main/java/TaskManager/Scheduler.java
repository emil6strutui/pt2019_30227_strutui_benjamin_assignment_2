package TaskManager;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;


    public Scheduler(int numberOfServers)
    {

      servers= new ArrayList<Server>();
      for(int i=0;i<numberOfServers;i++)
          servers.add(new Server());
    }

    public int bestServer()
    {   int a=0;
        int time=9999999;
        for(Server b:servers)
        {
            if (b.getWaitingPeriod().get()<time)
            {
                time=b.getWaitingPeriod().get();
                a=servers.indexOf(b);
            }
        }
        return a;
    }
    public boolean isEmpty(){
        for(Server a:servers){
            if(!a.getTasks().isEmpty()) return false;
        }
        return true;
    }

    public void dispatchTask(Task t,int time,int timeLimit)
    {
        if(time<timeLimit)
        servers.get(bestServer()).addTask(t);
    }

    public List<Server> getServers() {
        return servers;
    }
}
