package TaskManager;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    public static boolean isRunning;

    public Server(){
        isRunning=true;
       tasks=new ArrayBlockingQueue<Task>(20);
        waitingPeriod=new AtomicInteger(0);
    }

    public void addTask(Task a)
    {
        tasks.add(a);
        waitingPeriod.set(waitingPeriod.get()+a.getProcessingTime());
    }
    public String theQueue()
    {  String a="";
        for(Task b:tasks)
            a=a+b.theTask();
       return a;
    }

    public BlockingQueue<Task> getTasks() {
        return tasks;
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void run() {
              while(isRunning)
              {
                  if(!tasks.isEmpty()) {
                      try {
                          while(tasks.element().getProcessingTime()>0) {
                              Thread.sleep(1 * 1000);
                              waitingPeriod.set(waitingPeriod.get() - 1);
                              tasks.element().setProcessingTime(tasks.element().getProcessingTime()-1);
                          }
                          tasks.take();
                      } catch (InterruptedException e) {
                          e.printStackTrace();
                      }
                  }
              }
    }

    @Override
    public String toString() {
        return "Server{" +
                  tasks +
                ", waitingPeriod=" + waitingPeriod +
                '}';
    }

}
