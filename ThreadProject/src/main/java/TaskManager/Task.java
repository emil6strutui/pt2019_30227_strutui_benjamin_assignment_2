package TaskManager;

public class Task {
    private int numberOfTask;
    private int arrivalTime;
    private int processingTime;

    public Task(int nr,int arrivalTime,int processingTime)
    {   numberOfTask=nr;
        this.arrivalTime=arrivalTime;
        this.processingTime=processingTime;
    }


    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    @Override
    public String toString() {
        return "Task "+numberOfTask+"{"+
                "arrivalTime=" + arrivalTime +
                ", processingTime=" + processingTime +
                '}';
    }

    public String theTask()
    {
        return "Task "+numberOfTask+" ";
    }
}
