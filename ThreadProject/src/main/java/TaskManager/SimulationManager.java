package TaskManager;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class SimulationManager implements Runnable {

    private int timeLimit;
    private int maxProcTime;
    private int minProcTime;
    private int numberOfServers;
    private int numbeOfClients;
    private JTextArea log;
    private Integer minimumTime=99999999;
    private Integer maximumTime=-1;
    private Float avgTime=0.0f;
    private Integer PeakAmount=0;
    private Integer PeakTime=0;
    private JTextField[] clients;
    private Scheduler scheduler;
    private List<Task> tasks;
    private JButton button;

    public SimulationManager(int timeLimit,int maxProcTime,int minProcTime,int numberOfClients,int numberOfServers,JTextArea log,JTextField[] clients,JButton button)
    {
        this.timeLimit=timeLimit;
        this.maxProcTime=maxProcTime;
        this.minProcTime=minProcTime;
        this.numbeOfClients=numberOfClients;
        this.numberOfServers=numberOfServers;
        scheduler=new Scheduler(numberOfServers);
        tasks=new ArrayList<Task>();
        generateTasks();
        this.log=log;
        this.clients=clients;
        this.button=button;
        for(Server a:scheduler.getServers())
        {
            (new Thread(a)).start();
        }
    }


    private void generateTasks()
    {
         for(int i=0;i<numbeOfClients;i++)
         {
             int arriveTime = (int)Math.abs(Math.random() * (timeLimit - 1) + 1);
             int processingTime = (int)Math.abs(Math.random() * (maxProcTime - minProcTime) + minProcTime);
             tasks.add(new Task(i+1,arriveTime, processingTime));
         }
    }

    private void setMinMaxAvg(int a)
    {
        int time;
        int peak=0;
        for(Server server:scheduler.getServers()) {
            time = 0;
            for (Task task : server.getTasks()) {
                time+=task.getProcessingTime();
                if (time < minimumTime)
                    minimumTime = time;
                if (server.getWaitingPeriod().get() > maximumTime)
                    maximumTime = server.getWaitingPeriod().get();
            }
            avgTime+=server.getWaitingPeriod().get();
            peak+=server.getWaitingPeriod().get();
        }

        if (peak>PeakAmount)
        {
            PeakAmount=peak;
            PeakTime=a;
        }

    }

    private void printInLog(int time){
        log.setText(log.getText()+"\nServerele la timpul:"+time+"\n");
        int i=0;
        for (Server server: scheduler.getServers()){

            log.setText(log.getText()+server+"\n");
            clients[i].setText(server.theQueue());
            i++;
        }
    }
    public void run() {
        int time = 0;
             while( !scheduler.isEmpty() || time < timeLimit ){
                 time++;
                 for (Task task : tasks){
                     if (task.getArrivalTime() == time){
                         scheduler.dispatchTask(task,time,timeLimit);
                     }
                 }
                 printInLog(time);
                 setMinMaxAvg(time);
                 try{
                     Thread.sleep(1000);
                 }
                 catch (InterruptedException e)
                 {
                     e.printStackTrace();
                 }
             }
             time++;
             printInLog(time);
             Server.isRunning=false;
             button.setEnabled(true);
            avgTime=avgTime/(numberOfServers*(time-1));
        JOptionPane.showMessageDialog(null, "The processing is done and the times were: Min:"+minimumTime+" Max:"+maximumTime+"Avg:"+avgTime+" and the Peak Time at:"+PeakTime, "SUCCES", JOptionPane.INFORMATION_MESSAGE);
        log.setText(log.getText()+"DONE!");
    }


}
